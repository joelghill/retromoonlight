# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Setup ##

Follow this guide: [https://github.com/TechWizTime/moonlight-retropie](https://github.com/TechWizTime/moonlight-retropie)

Then, on your raspberry pi:

  1. git clone https://https://bitbucket.org/joelghill/retromoonlight.git
  
  2. cd retromoonlight
  
  3. sudo bash ./Install.sh

## Use ##
Emulationstation should now have an entry for Steam (should maybe rename). There will be a single "Rom" called refresh. Use this to update your games list. After using refresh command you will need to reboot emulationstation.

